import { CaracteristicDto } from '../dto/caracteristics.dto';
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type UserDocument = User & Document;

@Schema()
export class User {
  @Prop({ required: true })
  id: string;
  @Prop()
  name: string;
  @Prop()
  lastname: string;
  @Prop()
  readonly caracteristics: CaracteristicDto[];
}

export const UserSchema = SchemaFactory.createForClass(User);