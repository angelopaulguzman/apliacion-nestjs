import { UserDto } from './dto/user.dto';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Schema, Types } from 'mongoose';
import { User, UserDocument } from './schema/user.schema';


@Injectable()
export class UserService {
    constructor(
      @InjectModel('User') readonly userModel: Model<UserDocument>,
    ) {}

    async getUsers(): Promise<UserDocument[]> {
      const Users = await this.userModel.find();
      return Users;
    }

    async createUser(userDto: UserDto): Promise<UserDto> {
      const newUser = new this.userModel(userDto);
      return newUser.save();
    }

    async getUserById(UserId: string): Promise<UserDocument> {
      const user = await this.userModel.findById(UserId);
      return user;
    }

    async deleteUserById(UserId: string): Promise<any> {
      const deletedUser = await this.userModel.findByIdAndDelete(UserId);
      return deletedUser;
    }

    async updateUser(
      UserId: string,
      user: UserDto,
    ): Promise<User> {
      const updateUser = await this.userModel.findByIdAndUpdate(
        UserId,
        user,
      );
      return updateUser;
    }
}