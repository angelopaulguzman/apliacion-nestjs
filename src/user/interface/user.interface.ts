import { Document } from "mongoose";
import { CaracteristicDto } from "../dto/caracteristics.dto";
export interface UserInterface extends Document{
    readonly id: string;
    readonly name: string;
    readonly lastname: string;
   
    readonly caracteristics:CaracteristicDto[]; 
}