import { CaracteristicDto } from './caracteristics.dto';
export class UserDto{
    readonly id: string;
    readonly name: string;
    readonly lastname: string;
   
    readonly caracteristics:CaracteristicDto[];       
}

