import { UserDto } from './dto/user.dto';
import { UserService } from './user.service';
import {Query, Body, Controller, Delete, Get, HttpStatus, Param, Post, Res, Put } from '@nestjs/common';



@Controller('user')
export class UserController {
    constructor(private userService: UserService) {}

    @Get('/')
    async getAllUsers(@Res() res) {
      const users = await this.userService.getUsers();
      return res.status(HttpStatus.OK).json(users);
    }

    @Post('/')
    async createUser(@Res() res, @Body() user: UserDto) {
      try {
        const userResponse = await this.userService.createUser(user);
        return res
          .status(HttpStatus.ACCEPTED)
          .json({ message: 'El usuario fue creado' });
      } catch (e) {
        return res
          .status(HttpStatus.BAD_REQUEST)
          .json({ message: e.errors.name.message });
      }
    }

    @Get('/:userId')
    async getUserById(@Res() res, @Param('userId') userId: string) {
    if (userId.length !== 24)
      return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'el usuario no existe' });
    const user = await this.userService.getUserById(userId);
    if (!user)
      return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'el usuario no existe' });
    return res.status(HttpStatus.OK).json(user);
  }


  @Delete('/delete')
  async deleteUser(@Res() res, @Query('id') userId: string) {
    if (userId.length !== 24)
      return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'el usurio no existe' });
    const userDeleted = await this.userService.deleteUserById(
      userId,
    );
    if (!userDeleted)
      return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'el usuario no existe' });
    return res
      .status(HttpStatus.ACCEPTED)
      .json({ message: 'usuario ha sido eliminado con exito' });
  }

  @Delete('/:userId')
  async deleteUserById(@Res() res, @Param('userId') userId: string) {
    if (userId.length !== 24)
      return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'el Usuario no existe' });
    const userDeleted = await this.userService.deleteUserById(
      userId,
    );
    if (!userDeleted)
      return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'el usuario no existe' });
    return res
      .status(HttpStatus.ACCEPTED)
      .json({ message: 'usuario eliminado con exito' });
  }


  @Put('/:userId')
  async editUser(
    @Res() res,
    @Param('userId') userId: string,
    @Body() user: UserDto,
  ) {
    if (userId.length !== 24)
      return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'el usuario no existe!' });
    const updateUser = await this.userService.updateUser(
      userId,
      user,
    );
    if (!updateUser)
      return res
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'el Usuario no existe' });
    return res
      .status(HttpStatus.ACCEPTED)
      .json({ message: 'Usuario se edito con exito' });
  }


}
